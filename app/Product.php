<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

Class Product extends Model{
    protected $table = 'products';
    protected $fillable = [ 'name', 'price', 'description' ];
    protected $hidden = [ ];
}


